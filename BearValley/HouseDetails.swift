//
//  HouseDetails.swift
//  BearValley
//
//  Created by sainathreddy on 4/2/17.
//  Copyright © 2017 Arikapudi,Haritha. All rights reserved.
//

import Foundation

class HouseDetails{
    var name: String
    var detials: String
    var owner: String
    
    init(name: String, details: String, owner:String) {
        
        self.detials = details
        self.name = name
        self.owner = owner
    }
    
}
