//
//  LoginViewController.swift
//  BearValley
//
//  Created by Arikapudi,Haritha on 3/29/17.
//  Copyright © 2017 Arikapudi,Haritha. All rights reserved.
//

import UIKit
import Parse
import Bolts

class LoginViewController: UIViewController {
    var regUser:[PFObject]!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var password: UITextField!

    @IBAction func login(_ sender: Any) {
        
        if userName.text == "" || password.text == "" {
            self.displayAlertMessage(msg: "Please enter the details")
        }
        
        
        let query = PFQuery(className:"NewUser")
        
        query.findObjectsInBackground { (objects: [PFObject]?, error: Error?) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                
                // tranversing through the retrieved objects.
                for object in objects! {
                    // print(object)
                    
                    let username = object["userName"] as? String
                    let paswd = object["password"] as? String
                    print("\(username!) \(paswd!)")
               
                    if username == self.userName.text! && paswd == self.password.text! {
                        print("successful")

                        self.goto()

                        break
                        
                    }
                  
               
                }
                
            }
            else {
                self.displayAlertMessage(msg: "Wrong credientals! Try again")

            }
        }
    }
    func displayAlertMessage(msg:String){
        let myAlert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:nil)
        myAlert.addAction(okAction)
        
        self.present(myAlert,animated:true,completion:nil)
        
        
    }
    func goto(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "after") as! AfterLoginViewController
        self.present(nextViewController, animated:true, completion:nil)
    }

}
