//
//  RegisterViewController.swift
//  BearValley
//
//  Created by Arikapudi,Haritha on 3/29/17.
//  Copyright © 2017 Arikapudi,Haritha. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var userName: UITextField!

    @IBOutlet weak var email: UITextField!
   
    @IBOutlet weak var password: UITextField!
  
    @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBAction func reset(_ sender: Any) {
        userName.text = ""
        email.text = ""
        password.text = ""
        phoneNumber.text = ""
        confirmPassword.text = ""
        
    }
    
    @IBAction func register(_ sender: Any) {
        
        if userName.text! == "" || email.text! == "" || password.text! == "" || phoneNumber.text! == "" || confirmPassword.text! == "" {
            self.displayAlertMessage(msg: "Please enter all the fields")
        }

        let reg = PFObject(className: "NewUser")
        
        reg["emailId"] = email.text
        reg["userName"] = userName.text
        reg["phoneNumber"] = phoneNumber.text
        reg["password"] = password.text
        reg["confirmPassword"] = confirmPassword.text
        reg["firstName"] = firstName.text
        reg["lastName"] = lastName.text
        
        reg.saveInBackground { (success, error) -> Void in
            if success{
                //self.displayAlertMessage(msg: "success")
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login2")
                       self.present(nextViewController, animated:true, completion:nil)
                //self.performSegue(withIdentifier: "goBack", sender: sender)
            }
            else{
                print("error")
            }
            
        }
        

    }
    
    
    func displayAlertMessage(msg:String){
        let myAlert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title:"OK", style:UIAlertActionStyle.default, handler:nil)
        myAlert.addAction(okAction)
        
        self.present(myAlert,animated:true,completion:nil)
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "main") as! LoginViewController
//        self.present(nextViewController, animated:true, completion:nil)
        
        
    }

}
